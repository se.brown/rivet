BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y01
Title=$D_s^+\to K^0 e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$\text{d}\Gamma/\text{d}q^2$ [$\text{ns}^{-1}\text{GeV}^{-2}$]
LogY=0
END PLOT
