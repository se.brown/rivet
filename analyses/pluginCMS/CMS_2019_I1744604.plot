BEGIN PLOT /CMS_2019_I1744604/d13-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
YMin=0.0001
YMax=0.15
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d15-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $|y|$
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y| ~\textrm{(pb)}$
YMin=0.0
YMax=6.0
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d17-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
YMin=0.001
YMax=0.35
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d19-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $|y|$
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y| ~\textrm{(pb)}$
YMin=0.0
YMax=3.7
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d21-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level W $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
YMin=0.001
YMax=0.1
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d23-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level $\cos\theta_{\textrm{pol}}^{\star}$
YLabel=$\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}\cos\theta_{\textrm{pol}}^{\star} ~\textrm{(pb)}$
YMin=0.0
YMax=6.0
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT




BEGIN PLOT /CMS_2019_I1744604/d37-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $p_{\textrm{T}}$ (GeV)
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(1/GeV)}$
YMin=0.0001
YMax=0.025
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d39-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $|y|$
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y|$
YMin=0.0
YMax=1.0
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d41-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $p_{\textrm{T}}$ (GeV)
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(1/GeV)}$
YMin=0.0001
YMax=0.08
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d43-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $|y|$
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y|$
YMin=0.0
YMax=0.7
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d45-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level W $p_{\textrm{T}}$ (GeV)
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(1/GeV)}$
YMin=0.0001
YMax=0.02
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d47-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level $\cos\theta_{\textrm{pol}}^{\star}$
YLabel=$1/\sigma_{\textrm{t+}\bar{\textrm{t}}}\times\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}\cos\theta_{\textrm{pol}}^{\star}$
YMin=0.0
YMax=1.2
RatioPlotYMin=0.55
RatioPlotYMax=1.45
LogY=0
END PLOT




BEGIN PLOT /CMS_2019_I1744604/d59-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $p_{\textrm{T}}$ (GeV)
YLabel=$(\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}})/(\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}})$
YMin=0.15
YMax=0.9
RatioPlotYMin=0.75
RatioPlotYMax=1.25
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d61-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $|y|$
YLabel=$(\textrm{d}\sigma_{\textrm{t}}/\textrm{d}|y|)/(\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y|)$
YMin=0.15
YMax=0.9
RatioPlotYMin=0.75
RatioPlotYMax=1.25
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d63-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $p_{\textrm{T}}$ (GeV)
YLabel=$(\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}})/(\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}})$
YMin=0.15
YMax=0.9
RatioPlotYMin=0.75
RatioPlotYMax=1.25
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d65-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $|y|$
YLabel=$(\textrm{d}\sigma_{\textrm{t}}/\textrm{d}|y|)/(\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}|y|)$
YMin=0.15
YMax=0.9
RatioPlotYMin=0.75
RatioPlotYMax=1.25
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/d67-x01-y01
Title=CMS, 13\,TeV, $\textrm{t}+\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level W $p_{\textrm{T}}$ (GeV)
YLabel=$(\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}})/(\textrm{d}\sigma_{\textrm{t+}\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}})$
YMin=0.15
YMax=0.9
RatioPlotYMin=0.75
RatioPlotYMax=1.25
LogY=0
END PLOT







BEGIN PLOT /CMS_2019_I1744604/t_top_pt
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_top_pt
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/t_top_y
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $|y|$
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}|y| ~\textrm{(pb)}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_top_y
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level top quark $|y|$
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}|y| ~\textrm{(pb)}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/t_lepton_pt
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_lepton_pt
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/t_lepton_y
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $|y|$
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}|y| ~\textrm{(pb)}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_lepton_y
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level lepton $|y|$
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}|y| ~\textrm{(pb)}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/t_w_pt
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level W $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_w_pt
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level W $p_{\textrm{T}}$ (GeV)
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}p_{\textrm{T}} ~\textrm{(pb/GeV)}$
LogY=1
END PLOT

BEGIN PLOT /CMS_2019_I1744604/t_top_cos
Title=CMS, 13\,TeV, $\textrm{t}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level $\cos\theta_{\textrm{pol}}^{\star}$
YLabel=$\textrm{d}\sigma_{\textrm{t}}/\textrm{d}\cos\theta_{\textrm{pol}}^{\star} ~\textrm{(pb)}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2019_I1744604/tbar_top_cos
Title=CMS, 13\,TeV, $\bar{\textrm{t}}$, $\textrm{e}/\mu +\mathrm{jets}$
XLabel=Particle-level $\cos\theta_{\textrm{pol}}^{\star}$
YLabel=$\textrm{d}\sigma_{\bar{\textrm{t}}}/\textrm{d}\cos\theta_{\textrm{pol}}^{\star} ~\textrm{(pb)}$
LogY=0
END PLOT

