BEGIN PLOT /MARKII_1979_I144382/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\psi(2S)\to J/\psi\pi^+\pi^-$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-2}$]
XLabel=$m^2_{\pi^+\pi^-}$ [$\text{GeV}^2$]
LogY=0
LegendXPos=0.2
END PLOT
