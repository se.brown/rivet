BEGIN PLOT /BABAR_2007_I746745/d01-x01-y01
Title=Spectrum for $\Omega_c^{0}$ production
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I746745/d02-x01-y01
Title=Branching ratio $B\to\Omega^-_cX$ with $\Omega_c^-\to\Omega^-\pi^+$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I746745/d03-x01-y01
Title=Cross section $e^+e^-\to\Omega^-_cX$ with $\Omega_c^-\to\Omega^-\pi^+$
YLabel=$\sigma\times\text{Br}$ [fb]
LogY=0
END PLOT
