BEGIN PLOT /CLEOII_2002_I606309/
LogY=0
END PLOT

BEGIN PLOT /CLEOII_2002_I606309/d03-x01-y01
Title=$J/\psi$ momentum distribution
XLabel=$p$ [GeV]
YLabel=$\text{d}B/\text{d}p$ [$\%/\text{GeV}$]
END PLOT

BEGIN PLOT /CLEOII_2002_I606309/d03-x01-y02
Title=$\psi(2S)$ momentum distribution
XLabel=$p$ [GeV]
YLabel=$\text{d}B/\text{d}p$ [$\%/\text{GeV}$]
END PLOT

BEGIN PLOT /CLEOII_2002_I606309/d04-x01-y01
Title=$J/\psi$ Helicity angle
XLabel=$\cos\theta$
YLabel=$1/B\text{d}B/\text{d}\cos\theta$
END PLOT
BEGIN PLOT /CLEOII_2002_I606309/d04-x01-y02
Title=$\psi(2S)$ Helicity angle
XLabel=$\cos\theta$
YLabel=$1/B\text{d}B/\text{d}\cos\theta$
END PLOT

BEGIN PLOT /CLEOII_2002_I606309/d01-x01-y01
Title=$J/\psi$ Polarization (all momentum)
XLabel=$p$ [GeV]
YLabel=$\alpha$
END PLOT
BEGIN PLOT /CLEOII_2002_I606309/d01-x01-y02
Title=$\psi(2S)$ Polarization (all momentum)
XLabel=$p$ [GeV]
YLabel=$\alpha$
END PLOT
BEGIN PLOT /CLEOII_2002_I606309/d02-x01-y01
Title=$J/\psi$ Polarization vs momentum
XLabel=$p$ [GeV]
YLabel=$\alpha$
END PLOT
