# BEGIN PLOT /ATLAS_2016_I1487726/d02-x01-y01
LogY=0
LegendXPos=0.05
Title=$\Delta R$ between lepton and closest jet, $p_\mathrm{T}^\text{lead}>500\,\text{GeV}$
XLabel=$\Delta R(\mu,j)$
YLabel=d$\sigma$/d$\Delta R$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1487726/d04-x01-y01
LogY=0
LegendXPos=0.05
Title=$\Delta R$ between lepton and closest jet, $500\,\text{GeV}<p_\mathrm{T}^\text{lead}<600\,\text{GeV}$
XLabel=$\Delta R(\mu,j)$
YLabel=d$\sigma$/d$\Delta R$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1487726/d05-x01-y01
LogY=0
Title=$\Delta R$ between lepton and closest jet, $p_\mathrm{T}^\text{lead}>650\,\text{GeV}$
XLabel=$\Delta R(\mu,j)$
YLabel=d$\sigma$/d$\Delta R$ [fb]
LegendXPos=0.1
LegendYPos=0.25
# END PLOT
