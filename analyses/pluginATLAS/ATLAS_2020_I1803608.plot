BEGIN PLOT /ATLAS_2020_I1803608/d..
LegendAlign=l
LegendXPos=0.05
LegendYPos=0.25
XTwosidedTicks=1
YTwosidedTicks=1
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d01
Title=Electroweak $Zjj$ (SR)
XLabel=$m_{jj}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d02
Title=Electroweak $Zjj$ (SR)
XLabel=$|\Delta y_{jj}|$
YLabel=$\text{d}\sigma / \text{d}|\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d03
Title=Electroweak $Zjj$ (SR)
XLabel=$p_{\text{T},\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}p_{\text{T},\ell\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d04
Title=Electroweak $Zjj$ (SR)
XLabel=$\Delta\phi_{jj}$
YLabel=$\text{d}\sigma / \text{d}\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d05
Title=Inclusive $Z+2$ jets (SR)
XLabel=$m_{jj}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d06
Title=Inclusive $Z+2$ jets (SR)
XLabel=$|\Delta y_{jj}|$
YLabel=$\text{d}\sigma / \text{d}|\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d07
Title=Inclusive $Z+2$ jets (SR)
XLabel=$p_{\text{T},\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}p_{\text{T},\ell\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d08
Title=Inclusive $Z+2$ jets (SR)
XLabel=$\Delta\phi_{jj}$
YLabel=$\text{d}\sigma / \text{d}\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d09
Title=Inclusive $Z+2$ jets (CRa)
XLabel=$m_{jj}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d10
Title=Inclusive $Z+2$ jets (CRa)
XLabel=$|\Delta y_{jj}|$
YLabel=$\text{d}\sigma / \text{d}|\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d11
Title=Inclusive $Z+2$ jets (CRa)
XLabel=$p_{\text{T},\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}p_{\text{T},\ell\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d12
Title=Inclusive $Z+2$ jets (CRa)
XLabel=$\Delta\phi_{jj}$
YLabel=$\text{d}\sigma / \text{d}\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d13
Title=Inclusive $Z+2$ jets (CRb)
XLabel=$m_{jj}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d14
Title=Inclusive $Z+2$ jets (CRb)
XLabel=$|\Delta y_{jj}|$
YLabel=$\text{d}\sigma / \text{d}|\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d15
Title=Inclusive $Z+2$ jets (CRb)
XLabel=$p_{\text{T},\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}p_{\text{T},\ell\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d16
Title=Inclusive $Z+2$ jets (CRb)
XLabel=$\Delta\phi_{jj}$
YLabel=$\text{d}\sigma / \text{d}\Delta\phi_{jj}$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d17
Title=Inclusive $Z+2$ jets (CRc)
XLabel=$m_{jj}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d18
Title=Inclusive $Z+2$ jets (CRc)
XLabel=$|\Delta y_{jj}|$
YLabel=$\text{d}\sigma / \text{d}|\Delta y_{jj}|$ [fb]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d19
Title=Inclusive $Z+2$ jets (CRc)
XLabel=$p_{\text{T},\ell\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d}p_{\text{T},\ell\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2020_I1803608/d20
Title=Inclusive $Z+2$ jets (CRc)
XLabel=$\Delta\phi_{jj}$
YLabel=$\text{d}\sigma / \text{d}\Delta\phi_{jj}$ [fb]
END PLOT

