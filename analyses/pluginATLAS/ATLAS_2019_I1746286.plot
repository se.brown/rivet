# BEGIN PLOT /ATLAS_2019_I1746286/.*
LegendAlign=r
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d01-x01-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d04-x01-y01
Title= $|\eta|$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d03-x01-y01
Title= Energy for $K^{0}_{S}$ inside $b$-jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d05-x01-y01
Title= Multiplicity for $K^{0}_{S}$ inside $b$-jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d02-x01-y01
Title= $x_{K}$ for $K^{0}_{S}$ inside $b$-jets
XLabel=$x_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/dx_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d06-x01-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d09-x01-y01
Title= $|\eta|$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d08-x01-y01
Title= Energy for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d10-x01-y01
Title= Multiplicity for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d07-x01-y01
Title= $x_{K}$ for $K^{0}_{S}$ inside non-$b$-jets
XLabel=$x_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/dx_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d11-x01-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ outside jets
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d13-x01-y01
Title= $|\eta|$ for $K^{0}_{S}$ outside jets
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d12-x01-y01
Title= Energy for $K^{0}_{S}$ outside jets
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d14-x01-y01
Title= Multiplicity for $K^{0}_{S}$ outside jets
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT


# BEGIN PLOT /ATLAS_2019_I1746286/d15-x01-y01
Title= $p_\text{T}$ for $K^{0}_{S}$ total sample
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d17-x01-y01
Title= $|\eta|$ for $K^{0}_{S}$ total sample
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d16-x01-y01
Title= Energy for $K^{0}_{S}$ total sample
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{K}/\text{d}E$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d18-x01-y01
Title= Multiplicity for $K^{0}_{S}$ total sample
XLabel=$N_{K}$
YLabel=$(1/N_\text{evt}) \text{d}N_\text{evt}/\text{d}N_{K}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d19-x01-y01
Title= $p_\text{T}$ for $\Lambda$ total sample
XLabel=$p_\text{T}$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}p_\text{T}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d21-x01-y01
Title= $|\eta|$ for $\Lambda$ total sample
XLabel=$|\eta|$
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1746286/d20-x01-y01
Title= Energy for $\Lambda$ total sample
XLabel=$E$ [GeV]
YLabel=$(1/N_\text{evt}) \text{d}N_{\Lambda}/\text{d}E$ [GeV$^{-1}$]
# END PLOT


