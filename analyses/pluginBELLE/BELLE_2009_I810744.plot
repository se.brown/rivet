BEGIN PLOT /BELLE_2009_I810744/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Upsilon(4S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{MeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [GeV]
LogY=0
END PLOT
