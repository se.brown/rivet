BEGIN PLOT /BELLE_2010_I835104/d01-x01-y01
Title=Mass distribution for $X_s$ in $B\to\eta X_s$
XLabel=$m_X$ [GeV]
YLabel=$\text{d}B/\text{d}m_X$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
